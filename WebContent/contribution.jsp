<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="line">

		<div class="main-contents">
			<a href="./">ホーム</a> <a href="logout">ログアウト</a>

		</div>
		<h1>新規投稿</h1>
		<div class="profile">
			<div class="name">
				<c:out value="${loginUser.name}" />
			</div>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
					<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" /><br>
					</c:forEach>

			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="form-area"><br />
			<form action="contribution" method="post">
				<input type="hidden" name="id" value="${loginUser.id}">
				件名（30文字以下）<br /> <input type="text" name="subject"
					value="${subject}"> <br /> <br />カテゴリー(10文字以下)<br /> <input
					type="text" name="category" value="${category}"><br /> <br />
				本文（1000文字以下）<br />
				<textarea name="message" cols="50" rows="7" class="tweet-box"><c:out
						value="${message}" /></textarea>
				<br /> <input type="submit" value="投稿">
			</form><br />
		</div><br />
	</div>

	<footer>
		<p class="copyright">Copyright(c)Akiyama Yoshiki</p>
	</footer>

</body>
</html>
