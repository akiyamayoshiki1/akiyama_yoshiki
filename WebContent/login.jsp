<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">

</head>
<body>
<div class="line">

<h2>ログイン</h2>
<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">

			<c:forEach items="${errorMessages}" var="message">
				<c:out value="${message}" />
			</c:forEach>

	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>




<form action="login" method="post"><br />
	<label for="login_id">ログインＩＤ</label>
	<input name="login_id" id="login_id" value="${login_id2}"/> <br />

	<label for="password">パスワード</label>
	<input name="password" type="password" id="password"/> <br />

	<input type="submit" value="ログイン" /> <br />

</form>
</div>
	<footer>
<p class="copyright">Copyright(c)Akiyama Yoshiki	</p>
</footer>


</body>
</html>
