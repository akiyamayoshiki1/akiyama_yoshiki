<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE>
<html>
<head>

<script type="text/javascript">
	function spellofrevival(stop) {
		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm(stop)) {
			return true;
		}
		// 「OK」時の処理終了
		// 「キャンセル」時の処理開始
		else {
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
	}

	function spellofstop(resurrection) {
		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm(resurrection)) {
			return true;
		}
		// 「OK」時の処理終了
		// 「キャンセル」時の処理開始
		else {
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
	}
</script>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<div class="line">
		<div class="main-contents">
			<a href="./">ホーム</a> <a href="signup">ユーザー新規登録</a> <a href="logout">ログアウト</a>
		</div>

		<div class="profile">
			<h1>ユーザー管理</h1>
		</div>
		<br>

		<c:if test="${ not empty errorMessages }">
			<c:forEach items="${errorMessages}" var="errorMessages">
				<div class="errorMessages">
					<c:out value="${errorMessages}" />
				</div>
			</c:forEach>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<table border="1" align="center"  style="background-color:#ECECFF;">
			<tr>
				<th>ログインＩＤ</th>
				<th>名前</th>
				<th>支店名</th>
				<th>役職名</th>
				<th>アカウント状態</th>
			</tr>
			<c:forEach items="${messages}" var="message">
				<tr>
					<td align="center"><span class="id"><a href="setting?ID=${message.id}"><c:out
									value="${message.login_id}" /></a></span></td>
					<td align="center"><span class="id"><c:out value="${message.name}" /><br></span>
					</td>
					<td align="center"><c:forEach items="${branch}" var="branch">
							<c:if test="${message.branch_id == branch.id}" var="flg2" />
							<c:if test="${flg2}">
								<span class="id"><c:out value="${branch.name}" /></span>
							</c:if>
						</c:forEach></td>
					<td align="center"><c:forEach items="${affiliations}" var="affiliations">
							<c:if test="${message.affiliation_id == affiliations.id}"
								var="flg" />
							<c:if test="${flg}">
								<span class="id"><c:out value="${affiliations.name}" /></span>
							</c:if>
						</c:forEach></td>
					<td align="center">
					<c:if test="${loginUser.id != message.id}" var="flg" /> <c:if
							test="${flg}">
							<form action="management" method="post"
								onClick="return spellofrevival('アカウントを停止させますか？')">
								<input type="hidden" value="${message.id}" name="id" /> <input
									type="hidden" value="${message.is_stoped}" name="account" />
								<c:if test="${message.is_stoped == 0}" var="flg" />
								<c:if test="${flg}">
									<input type="submit" value="停止" />
								</c:if>
							</form>
						</c:if>

						<form action="management" method="post"
							onClick="return spellofstop('アカウントを復活させますか？')">
							<input type="hidden" value="${message.id}" name="id" /> <input
								type="hidden" value="${message.is_stoped}" name="account" />
							<c:if test="${message.is_stoped == 1}" var="flg2" />
							<c:if test="${flg2}">
								<input type="submit" value="復活" />
							</c:if>
						</form>
						</td>
				</tr>
			</c:forEach>
		</table><br>
	</div>

	<footer>
		<p class="copyright">Copyright(c)Akiyama Yoshiki</p>
	</footer>
</body>
</html>