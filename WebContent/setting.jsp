<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="line">

		<div class="main-contents">
			<a href="./">ホーム</a> <a href="management">ユーザー管理</a> <a href="logout">ログアウト</a>
		</div>
		<h1>ユーザー編集</h1>
		<h2>
			<c:out value="${loginUser.name}" />
		</h2>

		<div class="form-area">
			<form action="setting" method="post">
				<input type="hidden" name="id" value="${editUser.id}">
				<input type="hidden" name="upd" value="${editUser.updateDate}" /><br />

				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">

						<c:forEach items="${errorMessages}" var="message">
							<c:out value="${message}" />
						</c:forEach>

					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>

				<label for="name">名前</label> <input name="name"
					value="${editUser.name}" id="name"
				/><br /> <label for="login_id">ログインＩＤ</label> <input
					name="login_id" value="${editUser.login_id}"
				/><br> <label for="password">パスワード</label> <input
					name="password" type="password" id="password"
				/> <br /> <label for="password2">確認用パスワード</label> <input
					name="password2" type="password" id="password"
				/> <br />

				<c:if test="${loginUser.id == editUser.id}" var="flg" />
				<c:if test="${flg}">

					<input type="hidden" name="branch_id" value="${editUser.branch_id}">
					<br />
					<input type="hidden" name="affiliation_id"
						value="${editUser.affiliation_id}"
					>
					<br />

				</c:if>


				<c:if test="${loginUser.id != editUser.id}" var="flg2" />
				<c:if test="${flg2}">

					<label for="branch_id">支店名</label>
					<select name="branch_id">
						<c:forEach items="${branch}" var="branch">
							<c:if test="${branch.id == editUser.branch_id}">
								<option value="${branch.id}" selected="selected"><c:out
										value="${branch.name}"
									/></option>
							</c:if>
							<c:if test="${branch.id != editUser.branch_id}">
								<option value="${branch.id}"><c:out
										value="${branch.name}"
									/></option>
							</c:if>
						</c:forEach>
					</select>
					<br />
					<label for="affiliation_id">部署/役職名</label>
					<select name="affiliation_id">
						<c:forEach items="${affiliations}" var="affiliations">
							<c:if test="${affiliations.id == editUser.affiliation_id}">
								<option value="${affiliations.id}" selected="selected"><c:out
										value="${affiliations.name}"
									/></option>
							</c:if>
							<c:if test="${affiliations.id != editUser.affiliation_id}">
								<option value="${affiliations.id}"><c:out
										value="${affiliations.name}"
									/></option>
							</c:if>
						</c:forEach>
					</select>
					<br />

				</c:if>
				<br /> <input type="hidden" name="update_date"
					value="${editUser.updateDate}"
				> <input type="submit" value="登録" /> <br /> <br />
			</form>
		</div>
	</div>
	<footer>
		<p class="copyright">Copyright(c)Akiyama Yoshiki</p>
	</footer>

</body>
</html>
