<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="line">

<div class="main-contents">
<a href="./">ホーム</a>
<c:if test="${loginUser.branch_id == 1}" >
		<c:if test="${loginUser.affiliation_id == 1}" >
		 <a href="management">ユーザー管理</a>
		</c:if>
		 </c:if>
<a href="logout">ログアウト</a>
</div>

<h1>ユーザー新規登録</h1>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">

				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" /><br>
				</c:forEach>

		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
<div class="form-area">
		<form action="signup" method="post">
			<br /> <label for="name">名前</label> <input name="name" id="name"
				value="${signUpUser.name}" /><br /> <label for="login_id">ログインＩＤ</label>
			<input name="login_id" id="login_id" value="${signUpUser.login_id}" /><br />

			<label for="password">パスワード</label> <input name="password"
				type="password" id="password" /> <br /> <label for="password2">確認用パスワード</label>
			<input name="password2" type="password" id="password" /> <br />

			<label for="branch_id">支店名</label>
				 <select name="branch_id">
					<c:forEach items="${branch}" var="branch">
					<c:if test="${branch.id == signUpUser.branch_id}">
							<option value="${branch.id}" selected="selected"><c:out value="${branch.name}" /></option>
						</c:if>
						<c:if test="${branch.id != signUpUser.branch_id}">
							<option value="${branch.id}" ><c:out value="${branch.name}" /></option>
						</c:if>
					</c:forEach>
				</select><br />


			<label for="affiliation_id">部署/役職名</label>
			<select name="affiliation_id">
				<c:forEach items="${affiliations}" var="affiliations">
					<c:if test="${affiliations.id == signUpUser.affiliation_id}">
					<option value="${affiliations.id}" selected="selected"><c:out value="${affiliations.name}" /></option>
</c:if>
<c:if test="${affiliations.id != signUpUser.affiliation_id}">
					<option value="${affiliations.id}" ><c:out value="${affiliations.name}" /></option>
</c:if>

					</c:forEach>
			</select><br /> <br /> <input type="submit" value="登録" />  <input value="戻る"
				onclick="location.href='./management'" type="button"><br /><br /><br />
		</form>
	</div><br /></div>
<footer>
<p class="copyright">Copyright(c)Akiyama Yoshiki</p>
</footer>
</body>
</html>
