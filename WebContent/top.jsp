<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<script src="http//code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="./js/ajax.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript">
	function spellofrevival(stop) {
		// 「OK」時の処理開始 ＋ 確認ダイアログの表示
		if (window.confirm(stop)) {
			return true;
		}
		// 「OK」時の処理終了
		// 「キャンセル」時の処理開始
		else {
			window.alert('キャンセルされました'); // 警告ダイアログを表示
			return false;
		}
	}
</script>

<title>ホーム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="line">



		<div class="main-contents">
			<a href="./">ホーム</a>
			<c:if test="${loginUser.branch_id == 1}">
				<c:if test="${loginUser.affiliation_id == 1}">
					<a href="management">ユーザー管理</a>
				</c:if>
			</c:if>
			<a href="contribution">新規投稿</a> <a href="logout">ログアウト</a>

		</div>
		<br>
		<h1>ホーム</h1>

		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
		</div>

		<form action="./" method="get">
			<label for="narrowing">投稿絞込み</label> <label for="period">期間</label> <input
				type="date" name="start1" value="${starttime}">～ <input
				type="date" name="end1" value="${endtime}"> <br> <label
				for="ｃategory">カテゴリー</label> <input type="text" name="categ"
				value="${category}"><br> <input type="submit"
				value="検索" />
		</form>

		<c:if test="${ not empty errorMessages2 }">
			<div class="errorMessages2">

				<c:forEach items="${errorMessages2}" var="filtermessages">
					<c:out value="${filtermessages}" />
				</c:forEach>

		<a href="./"> <input type="submit" value="リセット" /></a>
			</div>
			<c:remove var="errorMessages2" scope="session" />
		</c:if>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">

				<c:forEach items="${errorMessages}" var="message">
					<c:out value="${message}" />
				</c:forEach>

			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

<br>

		<div class="messages">
			<c:forEach items="${messages1}" var="messages1">
<br>
				<div class="message"><div class="form-area">
					<span class="subject">◆件名：<c:out
							value="${messages1.subject}" /></span><br> <span class="category">カテゴリ：<c:out
							value="${messages1.category}" /></span><br> <span class="name">名前：<c:out
							value="${messages1.name}" /></span><br>
					<div class="date">
						日時：
						<fmt:formatDate value="${messages1.insert_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<span class="text">本文：</span><br>

					<c:forEach var="s" items="${fn:split(messages1.text, '
')}">
						<c:out value="${s}" />
						<br>
					</c:forEach>
</div>
					<c:if test="${loginUser.id == messages1.user_id}" var="flg" />
					<c:if test="${flg}">
						<form action="commentdelete" method="post">
							<input type="hidden" value="${messages1.id}" name="id" /> <input
								type="submit" value="削除"
								onClick="return spellofrevival('削除してもよろしいですか？')" />
						</form>
					</c:if>
					<br>
					<c:forEach items="${comment}" var="comment">
						<c:if test="${comment.message_id == messages1.id}" var="flg" />
						<c:if test="${flg}">


							<span class="text">コメント：</span>
							<br>
							<c:forEach var="s" items="${fn:split(comment.text, '
')}">
								<c:out value="${s}" />
								<br>
							</c:forEach>
							<span class="name"><c:out value="${comment.name}" /></span>
							<div class="date">
								日時：
								<fmt:formatDate value="${comment.insert_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<c:if test="${loginUser.id == comment.user_id}" var="flg2" />
							<c:if test="${flg2}">
								<form action="commentdelete2" method="post">
									<input type="hidden" value="${comment.id}" name="id5" /> <input
										type="submit" value="削除"
										onClick="return spellofrevival('削除してもよろしいですか？')" />
								</form>
							</c:if>
							<br>
						</c:if>
					</c:forEach>
					<c:if test="${ not empty errorMessages }">
						<div class="errorMessages">

							<c:forEach items="${errorMessages}" var="message">
								<c:out value="${message}" />
							</c:forEach>

						</div>
						<c:remove var="errorMessages" scope="session" />
					</c:if>
					<form action="comment" method="post">
						<input type="hidden" name="id" value="${messages1.id}" /> <label
							for="text">コメント投稿</label>

						<textarea name="text" cols="30" rows="6" class="tweet-box"><c:if
								test="${messages1.id == comment1.message_id}">
								<c:out value="${comment1.text}" />
							</c:if></textarea>
						<br>
						<input id="button" type="submit" value="投稿" />
					</form>
					<br /> <br> <br>

				</div>
			</c:forEach>
		</div>
	</div>
	<footer>
		<p class="copyright">Copyright(c)Akiyama Yoshiki</p>
	</footer>


</body>
</html>
