package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String login_id;
	private String name;
	private String password;
	private int branch_id;// text;
	private int affiliation_id;// insertDate;
	private int is_stoped;
	private String subject;
	private String category;
	private String text;
	private Timestamp insert_date;
	private int user_id;

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}

	public int getAffiliation_id() {
		return affiliation_id;
	}

	public void setAffiliation_id(int affiliation_id) {
		this.affiliation_id = affiliation_id;
	}

	public int getIs_stoped() {
		return is_stoped;
	}

	public void setIs_stoped(int is_stoped) {
		this.is_stoped = is_stoped;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Timestamp getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(Timestamp insertDate) {
		this.insert_date = insertDate;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}



}