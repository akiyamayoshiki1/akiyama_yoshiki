package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserMessage;
import beans.Usercomment;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = {"/comment" })

public class Comment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {



		List<Usercomment> comment1 = new CommentService().getComment();

		request.setAttribute("comment1", comment1);

		request.getRequestDispatcher("/index").forward(request, response);//これか
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();



		List<UserMessage> messages1 = new MessageService().getMessage();

		request.setAttribute("messages1", messages1);

		List<String> messages = new ArrayList<String>();
		Usercomment comment = new Usercomment();
		comment.setMessage_id(Integer.parseInt(request.getParameter("id")));//ここ
		comment.setText(request.getParameter("text"));
		User user = (User) session.getAttribute("loginUser");
		comment.setUser_id(user.getId());

		if (isValid(request, messages) == true) {

			new CommentService().register(comment);
			response.sendRedirect("./");
			//バリデーション
		} else {
/*			String message = request.getParameter("text");
			request.setAttribute("text", message);
*/



			session.setAttribute("comment1", comment);
			int messageid=Integer.parseInt(request.getParameter("id"));
			List<UserMessage> messages3 = new MessageService().getMessage4(messageid);
			request.setAttribute("messageid", messages3);
			System.out.println(comment.getMessage_id());
			System.out.println(messageid);
			System.out.println(comment.getText());
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");

		}

	}
//バリデーション
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("text");

		if (StringUtils.isBlank(message) == true) {
			messages.add("コメントを入力してください");
		}
		if (message.length() >= 501) {
			messages.add("コメントが５００文字を超えています");
		}


		if (messages.size() == 0) {

			return true;
		} else {
			return false;
		}


	}

}