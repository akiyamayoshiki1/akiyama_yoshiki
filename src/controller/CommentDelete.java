package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserMessage2;
import service.MessageService;

@WebServlet(urlPatterns = { "/commentdelete" })
public class CommentDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("/comment").forward(request, response);// これか
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {



		UserMessage2 message = new UserMessage2();

		message.setId(Integer.parseInt(request.getParameter("id")));

		new MessageService().register2(message);

		response.sendRedirect("./");

	}
}