package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Usercomment;
import service.CommentService;
@WebServlet(urlPatterns = { "/commentdelete2" })
public class CommentDelete2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("/comment").forward(request, response);// これか
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		Usercomment comment = new Usercomment();
		comment.setId(Integer.parseInt(request.getParameter("id5")));

		new CommentService().register2(comment);

		response.sendRedirect("./");

	}
}