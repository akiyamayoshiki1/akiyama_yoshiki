package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserMessage2;
import service.MessageService;

@WebServlet(urlPatterns = { "/contribution" })
public class Contribution extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("contribution.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			UserMessage2 message = new UserMessage2();
			message.setSubject(request.getParameter("subject"));
			message.setCategory(request.getParameter("category"));
			message.setText(request.getParameter("message"));
			message.setUser_id(user.getId());// これよ

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {

//			List<String> messages2 = new ArrayList<String>();
			String subject = request.getParameter("subject");
			String category = request.getParameter("category");
			String message = request.getParameter("message");
			/*messages2.add(subject);
			messages2.add(category);
			messages2.add(message);
			request.setAttribute("message", messages2);*/
			request.setAttribute("subject", subject);
			request.setAttribute("category",category);
			request.setAttribute("message", message);

			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("contribution.jsp").forward(request, response);
	//		response.sendRedirect("contribution.jsp");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("subject");
		String message2 = request.getParameter("category");
		String message3 = request.getParameter("message");

		if (StringUtils.isBlank(message) == true) {
			messages.add("件名を入力してください");
		}
		if (30 < message.length()) {
			messages.add("件名は30文字以下で入力してください");
		}
		if (StringUtils.isBlank(message2) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (10 < message2.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");

		}
		if (StringUtils.isBlank(message3) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < message3.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}