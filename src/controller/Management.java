package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserMessage;
import service.MessageService;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class Management extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		session.getAttribute("loginUser");


		List<UserMessage> messages = new MessageService().getMessage();

		List<User> branch = new UserService().getbranch();
		request.setAttribute("branch", branch);

		List<User> affiliations = new UserService().getaffiliations();
		request.setAttribute("affiliations", affiliations);

		request.setAttribute("messages", messages);

		request.getRequestDispatcher("management.jsp").forward(request, response);
	}



	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<User> branch = new UserService().getbranch();
		request.setAttribute("branch", branch);

		List<User> affiliations = new UserService().getaffiliations();
		request.setAttribute("affiliations", affiliations);

		User user = new User();

		if(Integer.parseInt(request.getParameter("account"))==0){
			//ここにメッセージを出す

			user.setId(Integer.parseInt(request.getParameter("id")));
			user.setIs_stoped(Integer.parseInt(request.getParameter("account")));

			new UserService().update2(user);

			response.sendRedirect("management");
		}


		if(Integer.parseInt(request.getParameter("account"))==1){

			user.setId(Integer.parseInt(request.getParameter("id")));
			user.setIs_stoped(Integer.parseInt(request.getParameter("account")));

			new UserService().update3(user);

			response.sendRedirect("management");
		}




	}


}