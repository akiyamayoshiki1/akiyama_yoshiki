package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.LoginService;
import service.SettingService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		String id = (request.getParameter("ID"));
		try {
			if (StringUtils.isEmpty(id) == false) {
				if (id.matches("^[0-9]*$") == true) {
					int paraid = (Integer.parseInt(request.getParameter("ID")));
					// HttpSession session = request.getSession();
					session.getAttribute("loginUser");
					User editUser = new SettingService().getUser(paraid);
					if (editUser != null) {
						List<User> branch = new UserService().getbranch();
						List<User> affiliations = new UserService().getaffiliations();
						request.setAttribute("branch", branch);
						request.setAttribute("affiliations", affiliations);
						request.setAttribute("editUser", editUser);
						String[] bran = request.getParameterValues("branch_id");
						System.out.println(bran);
						HttpSession session2 = request.getSession();
						session2.getAttribute("loginUser");
						session2.getAttribute("loginUser");

						request.getRequestDispatcher("setting.jsp").forward(request, response);

					} else {
						List<String> messages = new ArrayList<String>();
						messages.add("指定されたIDは存在しません");
						session.setAttribute("errorMessages", messages);
						response.sendRedirect("management");
						return;
					}
				} else {
					List<String> messages = new ArrayList<String>();
					messages.add("指定されたIDは存在しません");
					// HttpSession session = request.getSession();
					session.setAttribute("errorMessages", messages);
					response.sendRedirect("management");
					return;
				}
			} else {
				List<String> messages = new ArrayList<String>();
				messages.add("指定されたIDは存在しません");
				// HttpSession session = request.getSession();
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("management");
				return;
			}

		} catch (NumberFormatException e) {

			List<String> messages = new ArrayList<String>();
			messages.add("指定されたIDは存在しません");
			// HttpSession session = request.getSession();
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<User> branch = new UserService().getbranch();
		request.setAttribute("branch", branch);

		List<User> affiliations = new UserService().getaffiliations();
		request.setAttribute("affiliations", affiliations);

		// 11/28はここでフォームのデータを編集させるところから
		String update = request.getParameter("upd");
		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		User editUser = getEditUser(request, messages);// ここ

		if (isValid(request, messages) == true) {

			try {
				new SettingService().update(editUser, update, request);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);

			}
			User user = (User) request.getSession().getAttribute("loginUser");
			int id5 = (Integer.parseInt(request.getParameter("id")));

			if (id5 == user.getId()) {
				LoginService loginService = new LoginService();
				User user2 = loginService.edit(id5);
				HttpSession session2 = request.getSession();
				session2.setAttribute("loginUser", user2);
			}
			// request.setAttribute("loginUser", editUser);

			// session.getAttribute("loginUser");
			response.sendRedirect("management");
		} else {

			request.setAttribute("editUser", editUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request, List<String> messages) throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLogin_id(request.getParameter("login_id"));

		if (request.getParameter("password").length() != 0) {

			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");

			if (6 > password.length() || 20 <= password.length()) {
				messages.add("パスワードは6文字以上20文字以下で入力してください");
			}

			if (!password.equals(password2)) {
				messages.add("パスワードが一致しません");

			}
			// 半角英数字で正規表現
			if (!password.matches("^[a-zA-Z0-9!-/:-@\\[-`{-~]+$")) {
				messages.add("パスワードは半角英数字記号で入力してください");
			}

			editUser.setPassword(request.getParameter("password"));
		}

		editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		editUser.setAffiliation_id(Integer.parseInt(request.getParameter("affiliation_id")));

		Date formatDate = null;
		try {
			formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(request.getParameter("update_date"));
		} catch (ParseException e) {
			// フォーマット変換に失敗したらエラー
			e.printStackTrace();

		}

		editUser.setUpdateDate(formatDate);
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		int id = Integer.parseInt(request.getParameter("id"));
		User editUser = new SettingService().getUser(id);

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		LoginService loginService = new LoginService();
		User user = loginService.login2(login_id);
		String branch_id = request.getParameter("branch_id");
		String affiliation_id = request.getParameter("affiliation_id");

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}
		if (10 < name.length()) {
			messages.add("名前は10文字以下で入力してください");
		}
		// 半角英数字で正規表現
		// ここ途中あああああああああああああああああああ
		if (6 > login_id.length() || 20 <= login_id.length()) {
			if (StringUtils.isBlank(login_id) == true) {
				messages.add("ログインＩＤは6文字以上20文字以下で入力してください");
			} else if (!login_id.matches("^[a-zA-Z0-9]+$")) {
				messages.add("ログインＩＤは半角英数字で入力してください");
			}

		}
		if (user != null && user.getId() != editUser.getId()) {
			messages.add("指定されたログインＩＤは存在しています");
		}

		if (StringUtils.isBlank(branch_id) == true) {
			messages.add("支店を選択してください");
		}
		if (StringUtils.isBlank(affiliation_id) == true) {
			messages.add("部署/役職を選択してください");
		}

		if (branch_id.equals("1") && !affiliation_id.equals("1")) {
			if (!affiliation_id.equals("2")) {
				if (!affiliation_id.equals("4")) {
					messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
				}
			}
		}
		if (branch_id.equals("2") && !affiliation_id.equals("3")) {
			if (!affiliation_id.equals("4")) {
				messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
			}
		}
		if (branch_id.equals("3") && !affiliation_id.equals("3")) {
			if (!affiliation_id.equals("4")) {
				messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
			}
		}
		if (branch_id.equals("4") && !affiliation_id.equals("3")) {
			if (!affiliation_id.equals("4")) {
				messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
			}
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}