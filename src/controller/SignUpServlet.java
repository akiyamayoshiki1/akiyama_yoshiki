package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<User> branch = new UserService().getbranch();
		request.setAttribute("branch", branch);

		List<User> affiliations = new UserService().getaffiliations();
		request.setAttribute("affiliations", affiliations);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<User> branch = new UserService().getbranch();
		request.setAttribute("branch", branch);

		List<User> affiliations = new UserService().getaffiliations();
		request.setAttribute("affiliations", affiliations);

		List<String> messages = new ArrayList<String>();
		User signUpUser = getSignUpUser(request);

		if (isValid(request, messages) == true) {
			new UserService().register(signUpUser);

			response.sendRedirect("management");
		} else {

			request.setAttribute("signUpUser", signUpUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private User getSignUpUser(HttpServletRequest request) throws IOException, ServletException {

		User signUpUser = new User();
		signUpUser.setName(request.getParameter("name"));
		signUpUser.setLogin_id(request.getParameter("login_id"));
		signUpUser.setPassword(request.getParameter("password"));
		signUpUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		signUpUser.setAffiliation_id(Integer.parseInt(request.getParameter("affiliation_id")));
		return signUpUser;

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String branch_id = request.getParameter("branch_id");
		String affiliation_id = request.getParameter("affiliation_id");
		LoginService loginService = new LoginService();
		User user = loginService.login2(login_id);

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}
		if (10 < name.length()) {
			messages.add("名前は10文字以下で入力してください");
		}

		if (6 > login_id.length() || 20 <= login_id.length()) {
			if (StringUtils.isBlank(login_id) == true) {
				messages.add("ログインＩＤは6文字以上20文字以下で入力してください");
			} else if (!login_id.matches("^[a-zA-Z0-9]+$")) {
				messages.add("ログインＩＤは半角英数字で入力してください");
			}
		}
		if (user != null) {
			messages.add("指定されたログインＩＤは存在しています");
		}
		if (!password.equals(password2)) {
			messages.add("パスワードが一致しません");
		}
		if (6 > password.length() || 20 <= password.length()) {
			if (StringUtils.isBlank(password) == true) {
				messages.add("パスワードは6文字以上20文字以下で入力してください");
			} else if (!password.matches("^[a-zA-Z0-9!-/:-@\\[-`{-~]+$")) {
				messages.add("パスワードは半角英数字記号で入力してください");
			}
		}
		if (branch_id.equals("1") && !affiliation_id.equals("1")) {
			if (!affiliation_id.equals("2")) {
				if (!affiliation_id.equals("4")) {
					messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
				}
			}
		}
		if (branch_id.equals("2") && !affiliation_id.equals("3")) {
			if (!affiliation_id.equals("4")) {
				messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
			}
		}
		if (branch_id.equals("3") && !affiliation_id.equals("3")) {
			if (!affiliation_id.equals("4")) {
				messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
			}
		}
		if (branch_id.equals("4") && !affiliation_id.equals("3")) {
			if (!affiliation_id.equals("4")) {
				messages.add("支店ＩＤと部署役職コードの組み合わせが不正です");
			}
		}

		/*
		 * if (!branch_id.equals("1") && affiliation_id.equals("1")) { if
		 * (affiliation_id.equals("2")){
		 * messages.add("支店ＩＤと部署役職コードの組み合わせが不正です"); } }
		 */

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}