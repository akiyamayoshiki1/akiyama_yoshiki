package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserMessage;
import beans.Usercomment;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}
		String strt;
		String end;
		String strt1;
		String end1;
		String cate;
		String categ1;
		categ1=request.getParameter("categ");

		cate = "%"+categ1+"%";


		strt1 = request.getParameter("start1");//+" 00:00:00"
		end1= request.getParameter("end1");//+" 23:59:59"


		if(categ1 ==null || categ1.length()==0){
			cate="%%";
		}





		if(strt1==null || strt1.length()==0){
			strt="2017-11-20 00:00:00";
		}else{
			strt =strt1+" 00:00:00";//+" 00:00:00"
		}
		if(end1==null || end1.length()==0){
			  Date date = new Date();
		        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		      end=sdf1.format(date);
			}else{
				end= end1+" 23:59:59";
			}




System.out.println(strt);
System.out.println(end);

		List<UserMessage> messages1 = new MessageService().getMessage1(strt,end,cate);
		List<Usercomment> comment = new CommentService().getComment();
		request.setAttribute("starttime", strt1);
		request.setAttribute("endtime",end1);
		request.setAttribute("category", categ1);

		request.setAttribute("comment", comment);
		request.setAttribute("messages1", messages1);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("/top.jsp").forward(request, response);// これか
	}



}