package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Usercomment;
import exception.SQLRuntimeException;

public class CommentDao {

	public List<Usercomment> getUsercomment(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" comments.id");
			sql.append(", comments.message_id");
			sql.append(", comments.user_id");
			sql.append(", comments.text");
			sql.append(", users.name");
			sql.append(", comments.insert_date");
			sql.append(" FROM comments");
			sql.append(" inner join users");
			sql.append(" on comments.user_id = users.id ");
			sql.append("  ORDER BY comments.message_id  DESC;");
			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<Usercomment> ret = toUsercommentList1(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public void delete(Connection connection, Usercomment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE  ");
			sql.append(" FROM");
			sql.append(" comments");
			sql.append(" WHERE");
			sql.append(" id = ?"); // user_id
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}





	private List<Usercomment> toUsercommentList1(ResultSet rs)
			throws SQLException {

		List<Usercomment> ret = new ArrayList<Usercomment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int message_id = rs.getInt("message_id");
				String name = rs.getString("name");
				String text= rs.getString("text");
				int user_id=rs.getInt("user_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");

				Usercomment comment1 = new Usercomment();
				comment1.setId(id);
				comment1.setMessage_id(message_id);
				comment1.setName(name);
				comment1.setText(text);
				comment1.setUser_id(user_id);
				comment1.setInsert_date(insertDate);
				ret.add(comment1);
			}
			return ret;
		} finally {
			close(rs);
		}
	}



	public void insert(Connection connection, Usercomment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append(" message_id");
			sql.append(", user_id");
			sql.append(", text");
			sql.append(", insert_date");
			sql.append(") VALUES (");
			sql.append("?");//message_id
			sql.append(" , ?"); // user_id
			sql.append(", ?"); // text
			sql.append(", CURRENT_TIMESTAMP"); // insert_date
			sql.append(")");
			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getMessage_id());
			ps.setInt(2, comment.getUser_id());
			ps.setString(3, comment.getText());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}




