package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class SettingDao {
	/*
	 * public User getUser(Connection connection, String login_id, String
	 * password) {
	 *
	 * PreparedStatement ps = null; try { String sql =
	 * "SELECT * FROM users WHERE login_id = ?  AND password = ?";
	 *
	 * ps = connection.prepareStatement(sql); ps.setString(1, login_id);
	 * ps.setString(2, password);
	 *
	 * ResultSet rs = ps.executeQuery(); List<User> userList = toUserList(rs);
	 * if (userList.isEmpty() == true) { return null; } else if (2 <=
	 * userList.size()) { throw new IllegalStateException("2 <= userList.size()"
	 * ); } else { return userList.get(0); } } catch (SQLException e) { throw
	 * new SQLRuntimeException(e); } finally { close(ps); } }
	 */

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branch_id = rs.getInt("branch_id");
				int affiliation_id = rs.getInt("affiliation_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				Timestamp updateDate = rs.getTimestamp("update_date");

				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setName(name);
				user.setPassword(password);
				user.setBranch_id(branch_id);
				user.setAffiliation_id(affiliation_id);
				user.setInsertDate(insertDate);
				user.setUpdateDate(updateDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(",name");
			sql.append(",password");
			sql.append(",branch_id");
			sql.append(",affiliation_id");
			sql.append(",insert_date");
			sql.append(",update_date");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // name
			sql.append(", ?"); // email
			sql.append(", ?"); // password
			sql.append(", ?"); // description
			sql.append(", CURRENT_TIMESTAMP"); // insert_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setLong(4, user.getBranch_id());
			ps.setLong(5, user.getAffiliation_id());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user, String update, HttpServletRequest request) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("login_id = ?");
			sql.append(",name = ?");

			if (request.getParameter("password").length() != 0) {
				sql.append(",password = ?");
			}


			sql.append(",branch_id = ?");
			sql.append(",affiliation_id= ?");


			sql.append(",update_date = CURRENT_TIMESTAMP ");
			sql.append(" WHERE ");
			sql.append("id = ? ");
			sql.append("AND ");
			sql.append("update_date = ?");

			ps = connection.prepareStatement(sql.toString());

			if (request.getParameter("password").length() != 0) {

				ps.setString(1, user.getLogin_id());
				ps.setString(2, user.getName());
				ps.setString(3, user.getPassword());
				ps.setLong(4, user.getBranch_id());
				ps.setLong(5, user.getAffiliation_id());
				ps.setInt(6, user.getId());
				ps.setString(7, update);
			}else{

				ps.setString(1, user.getLogin_id());
				ps.setString(2, user.getName());
				ps.setLong(3, user.getBranch_id());
				ps.setLong(4, user.getAffiliation_id());
				ps.setInt(5, user.getId());
				ps.setString(6, update);

			}

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public User getUser(Connection connection, int paraid) {


		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, paraid);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);

		}
	}

}