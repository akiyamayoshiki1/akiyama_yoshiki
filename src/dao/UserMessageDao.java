package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" *");
			sql.append(" FROM users ");//こ↑こ↓
			sql.append(" ORDER BY  branch_id  ");
			sql.append(" , affiliation_id ;");
			ps = connection.prepareStatement(sql.toString());

System.out.println(ps.toString());
			ResultSet rs = ps.executeQuery();

			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> getUserMessages1(Connection connection, int num, String strt, String end,String cate) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" messages.id");
			sql.append(", users.name");
			sql.append(", messages.subject");
			sql.append(", messages.category");
			sql.append(", messages.text");
			sql.append(", messages.user_id");
			sql.append(", messages.insert_date ");
			sql.append(" FROM messages");
			sql.append(" inner join users");
			sql.append(" on users.id = messages.user_id");
			sql.append("  WHERE  ");
			sql.append(" messages.insert_date  >= ?");
			sql.append(" and");
			sql.append(" messages.insert_date  <= ?");
			sql.append(" and");
			sql.append(" messages.category  LIKE ?");
			sql.append("  ORDER BY messages.id  DESC;");
			ps = connection.prepareStatement(sql.toString());





			ps.setString(1,strt);
			ps.setString(2,end);
			ps.setString(3,cate);



			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList1(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int affiliation_id = rs.getInt("affiliation_id");
				int is_stoped = rs.getInt("is_stoped");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setLogin_id(login_id);
				message.setName(name);
				message.setBranch_id(branch_id);
				message.setAffiliation_id(affiliation_id);
				message.setIs_stoped(is_stoped);
				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}




	private List<UserMessage> toUserMessageList1(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String subject= rs.getString("subject");
				String category= rs.getString("category");
				String text= rs.getString("text");
				int user_id=rs.getInt("user_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");

				UserMessage messages1 = new UserMessage();
				messages1.setId(id);
				messages1.setName(name);
				messages1.setSubject(subject);
				messages1.setCategory(category);
				messages1.setText(text);
				messages1.setUser_id(user_id);
				messages1.setInsert_date(insertDate);
				ret.add(messages1);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<UserMessage> getUserMessages2(Connection connection, int limitNum, int messageid) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" id");
			sql.append(", subject");
			sql.append(", category");
			sql.append(", text");
			sql.append(", user_id");
			sql.append(", insert_date ");
			sql.append(" FROM messages");

			ps = connection.prepareStatement(sql.toString());

System.out.println(toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList2(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList2(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");

				String subject= rs.getString("subject");
				String category= rs.getString("category");
				String text= rs.getString("text");
				int user_id=rs.getInt("user_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");

				UserMessage messages1 = new UserMessage();
				messages1.setId(id);
				messages1.setSubject(subject);
				messages1.setCategory(category);
				messages1.setText(text);
				messages1.setUser_id(user_id);
				messages1.setInsert_date(insertDate);
				ret.add(messages1);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	}



