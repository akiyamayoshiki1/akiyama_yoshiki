package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = { "/management", "/signup", "/setting" })
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// chain.doFilter(request, response);が呼ばれたら呼ばれる

		// HttpSession session = (HttpSession)((HttpServletRequest)
		// request).getSession().getAttribute("loginUser");

		HttpSession session = ((HttpServletRequest) request).getSession();
		if (session != null) {

			User loginUser = (User) session.getAttribute("loginUser");
			if (loginUser != null) {

				int branch = loginUser.getBranch_id();
				int affiliation = loginUser.getAffiliation_id();

				// 直打ちでいいからbranch_idとaffiliation_idが1.1かどうかを見る
				if (affiliation != 1 || branch != 1) {

					((HttpServletResponse) response).sendRedirect("./");

					List<String> filtermessages = new ArrayList<String>();
					filtermessages.add("この画面は本社人事担当者のみアクセスが許可されています");
					session.setAttribute("errorMessages2", filtermessages);
					// System.out.println( filtermessages.get(0));

					return;
				}
			}

			chain.doFilter(request, response);
		}

	}

	@Override
	public void init(FilterConfig config) {// アプリケーション開始時に呼ばれる

	}

	@Override
	public void destroy() {// アプリケーション終了時に呼ばれる
	}

}