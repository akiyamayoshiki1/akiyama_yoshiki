package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// chain.doFilter(request, response);が呼ばれたら呼ばれる

		// HttpSession session = (HttpSession)((HttpServletRequest)
		// request).getSession().getAttribute("loginUser");

		Object session = ((HttpServletRequest) request).getSession().getAttribute("loginUser");

		String path = ((HttpServletRequest) request).getServletPath();

		if (session == null && !(path.equals("/login"))) {

			HttpSession session1 = ((HttpServletRequest) request).getSession();
			List<String> Errrormessages = new ArrayList<String>();
			Errrormessages.add("ログインして下さい");
			session1.setAttribute("errorMessages", Errrormessages);

			((HttpServletResponse) response).sendRedirect("login");
			return;
		}


		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig config) {// アプリケーション開始時に呼ばれる

	}

	@Override
	public void destroy() {// アプリケーション終了時に呼ばれる
	}

}