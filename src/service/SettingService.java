package service;
import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import beans.User;
import dao.SettingDao;
import utils.CipherUtil;

public class SettingService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			SettingDao userDao = new SettingDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user,String update, HttpServletRequest request) {

		Connection connection = null;
		try {
			connection = getConnection();
			if (request.getParameter("password").length() != 0) {
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
			}

			SettingDao  userDao = new SettingDao ();
			userDao.update(connection, user ,update,request);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getUser(int paraid) {

		Connection connection = null;
		try {
			connection = getConnection();

			SettingDao  userDao = new SettingDao ();
			User user = userDao.getUser(connection,paraid);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
